//
//  ColorViewController.swift
//  AppLesson4Colours
//
//  Created by Глеб Зобнин on 20.10.2023.
//

import UIKit

final class ColorViewController: UIViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let settingsVC = segue.destination as? SettingsViewController else { return }
        settingsVC.delegate = self
        settingsVC.initialColor = view.backgroundColor ?? UIColor.black
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "showSettings", sender: nil)
    }
}

extension ColorViewController: SettingsViewControllerDelegate {
    func sendColor(color: UIColor) {
        view.backgroundColor = color
    }
}
