//
//  ViewController.swift
//  AppLesson4Colours
//
//  Created by Глеб Зобнин on 29.09.2023.
//

import UIKit

protocol SettingsViewControllerDelegate: AnyObject {
    func sendColor(color: UIColor)
}

final class SettingsViewController: UIViewController {

    @IBOutlet weak var blueValueLabel: UILabel!
    @IBOutlet weak var greenValueLabel: UILabel!
    @IBOutlet weak var redValueLabel: UILabel!
    
    @IBOutlet weak var blueSliderOutlet: UISlider!
    @IBOutlet weak var greenSliderOutlet: UISlider!
    @IBOutlet weak var redSliderOutlet: UISlider!
    
    @IBOutlet var colorTextFields: [UITextField]!
    
    @IBOutlet weak var paletteView: UIView!
    
    var initialColor: UIColor!
    
    weak var delegate: SettingsViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paletteView.layer.cornerRadius = 25
        setInitialValues(with: initialColor)
        setPaletteColour()
        
        colorTextFields[0].text = String(format: "%.2f", redSliderOutlet.value)
        colorTextFields[1].text = String(format: "%.2f", greenSliderOutlet.value)
        colorTextFields[2].text = String(format: "%.2f", blueSliderOutlet.value)
        
        for colorTextField in colorTextFields {
            colorTextField.delegate = self
            addDoneButtonOnNumpad(textField: colorTextField)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) { //уборка клавиатуры по тапу на пустую часть экрана
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    @IBAction func doneButtonPressed() {
        delegate.sendColor(
            color: UIColor(
            red: CGFloat(redSliderOutlet.value),
            green: CGFloat(greenSliderOutlet.value),
            blue: CGFloat(blueSliderOutlet.value),
            alpha: 1)
        )
        dismiss(animated: true)
    }
    
     @IBAction func sliderAction(_ sender: UISlider) {
         switch sender {
         case redSliderOutlet:
             redValueLabel.text = String(format: "%.2f", redSliderOutlet.value)
             colorTextFields[0].text = String(format: "%.2f", redSliderOutlet.value)
             setPaletteColour()
         case blueSliderOutlet:
             blueValueLabel.text = String(format: "%.2f", blueSliderOutlet.value)
             colorTextFields[2].text = String(format: "%.2f", blueSliderOutlet.value)
             setPaletteColour()
         case greenSliderOutlet:
             greenValueLabel.text = String(format: "%.2f", greenSliderOutlet.value)
             colorTextFields[1].text = String(format: "%.2f", greenSliderOutlet.value)
             setPaletteColour()
         default:
             print("other error")
         }
     }
}
//MARK: Set the colour of palelle (paletteView)
extension SettingsViewController {
    private func setPaletteColour() {
        paletteView.backgroundColor = UIColor(
            red: CGFloat(redSliderOutlet.value),
            green: CGFloat(greenSliderOutlet.value),
            blue: CGFloat(blueSliderOutlet.value),
            alpha: 1
        )
    }
}
//MARK: Set initial values to labels
extension SettingsViewController {
    func setInitialValues(with color: UIColor) {
        let ciColor = CIColor(color: color)
        redSliderOutlet.value = Float(ciColor.red)
        greenSliderOutlet.value = Float(ciColor.green)
        blueSliderOutlet.value = Float(ciColor.blue)
        redValueLabel.text = String(format: "%.2f", redSliderOutlet.value)
        greenValueLabel.text = String(format: "%.2f", greenSliderOutlet.value)
        blueValueLabel.text = String(format: "%.2f", blueSliderOutlet.value)
    }
}
extension SettingsViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let value = Float(textField.text ?? "") else {
            return
        }
        if 0...1 ~= value {
            switch textField.tag {
            case 0:
                redSliderOutlet.value = value
                sliderAction(redSliderOutlet)
            case 1:
                greenSliderOutlet.value = value
                sliderAction(greenSliderOutlet)
            case 2:
                blueSliderOutlet.value = value
                sliderAction(blueSliderOutlet)
            default:
                showAlert(with: "Error!", and: "error with sliders tags")
            }
        } else {
            showAlert(with: "Error", and: "Value should be between 0 and 1")
            let errorReplacement: Float = 0.4
            switch textField.tag {
            case 0:
                textField.text = String(errorReplacement)
                redSliderOutlet.value = errorReplacement
                sliderAction(redSliderOutlet)
            case 1:
                textField.text = String(errorReplacement)
                greenSliderOutlet.value = errorReplacement
                sliderAction(greenSliderOutlet)
            case 2:
                textField.text = String(errorReplacement)
                blueSliderOutlet.value = errorReplacement
                sliderAction(blueSliderOutlet)
            default:
                showAlert(with: "Error!", and: "error with error")
            }
        }
    }
}

extension SettingsViewController {
    private func showAlert(with title: String, and message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true)
    }
    
    // Add Done Button Code
    private func addDoneButtonOnNumpad(textField: UITextField) {
        let keypadToolbar: UIToolbar = UIToolbar()
        // add a done button to the numberpad
        keypadToolbar.items =
        [
            UIBarButtonItem(title: "Done",
                            style: UIBarButtonItem.Style.done,
                            target: textField,
                            action: #selector(UITextField.resignFirstResponder)),
            UIBarButtonItem(
                barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, 
                target: self,
                action: nil)
        ]
        keypadToolbar.sizeToFit()
        // add a toolbar with a done button above the number pad
        textField.inputAccessoryView = keypadToolbar
    }
}
